### 4.1.0

- Feature: support syntax introduced by base-compiler 4.0.
- Chore: Add dependencies on base-compiler.

#### 4.0.9

- Chore: use system ^2.
- Enhancement: abort when the documention of a dcl function could not be parsed.

#### 4.0.8

- Chore: support compiler 3.0.

#### 4.0.7

- Chore: use newest system package version to fix Windows build.

#### 4.0.6 (BROKEN on Windows)

- Chore: use newest system package version to fix Windows build.

#### 4.0.5 (BROKEN on Windows)

- Fix: Use imports from `test` instead of old `Gast`.

#### 4.0.4 (BROKEN)

- Fix: posix being broken.

#### 4.0.3 (BROKEN)

- Chore: support base ^2.0

#### 4.0.2

- Fix: cap Gast ^0.4 to ^0.4.1 (0.4.2 doesn't work with clean-test-properties).

#### 4.0.1

- Chore: accept Gast ^0.3 and ^0.4 as a use dependency.

## 4.0.0

- Change: `Control.Bimap` is not imported automatically anymore for property tests.
- Enhancement: drop the clean-platform use dependency.

#### 3.1.2

- Chore: accept clean-platform ^v0.3 and ^v0.4 as dependency.

#### 3.1.1

- Chore: update platform dependency to require at least version 0.3.1 to include Windows fixes for processes/files/system environment necessary for running property tests on Windows.
- Chore: update Gast dependency to require at least version 0.2.1 as platform had a major upgrade and Gast depends on platform.

### 3.1.0

- Fix: fix compilation of _system when -fusion or -generic_fusion is
  given.
- Fix: fix -m option.

- Enhancement: compile _system with the given clm arguments before
  compiling tests, because clm does not do this itself.
- Enhancement: don't regenerate test modules if they already contain the right
  contents, to avoid recompilation.

- Feature: allow --directory/-d to be given multiple times.

## 3.0.0

- Initial version, switch to nitrile.
