module propertytester

import StdEnv

import Control.Applicative
from Control.Monad import class Monad(..), sequence, =<<, foldM, instance Monad ?
import Data.Either
import Data.Error
from Data.Func import $, mapSt, on, `on`, seqSt
import Data.Functor
import Data.List
import Data.Maybe
import Data.Tuple
import System.CommandLine
import System.Directory
import System.Environment
import System.File
import System.FilePath
import System.Options
import System.OS
import System.Process
import Text
import Text.Language

import Clean.Doc
import Clean.Parse
import Clean.Parse.Comments
from Clean.Types import :: Type(Func,Type), :: TypeContext, :: TypeRestriction,
	:: TypeVar, :: TVAssignment, :: TypeContext(TypeContext),
	allVars
from Clean.Types.Util import instance toString Type, instance == Type,
	assign, assignAll

from syntax import
	:: Ident{id_name},
	:: Module{mod_defs,mod_ident}

import cocl_helper

:: Options =
	{ directories      :: !?[FilePath]
	, modules          :: ![FilePath]
	, output_directory :: !FilePath
	, output_prefix    :: !String
	, includes         :: ![FilePath]
	, library_includes :: ![String]
	, clean_home       :: !FilePath
	, print_options    :: ![String]
	, test_options     :: ![String]
	, compile          :: !Bool
	, clm_args         :: ![String]
	, run              :: !Bool
	, verbosity        :: !Int
	, color            :: !Bool
	}

defaultOptions :: Options
defaultOptions =
	{ directories      = ?None
	, modules          = []
	, output_directory = "."
	, output_prefix    = "_Tests"
	, includes         = []
	, library_includes = ["Gast","Platform"]
	, clean_home       = "/opt/clean"
	, print_options    = []
	, test_options     = []
	, compile          = False
	, clm_args         = ["-nt", "-nr"]
	, run              = False
	, verbosity        = SUCCESS
	, color            = True
	}

Start w
// Command line
# ([prog:args],w) = getCommandLine w
# (clean_home,w) = appFst (fromMaybe "") $ getEnvironmentVariable "CLEAN_HOME" w
# opts = parseOptions optionDescription args {defaultOptions & clean_home=clean_home}
| isError opts = exit (join "\n" $ fromError opts) {defaultOptions & color=False} w
# opts = fromOk opts
# opts = if (opts.run && isMember "OutputTestEvents" opts.print_options)
	{opts & verbosity=0, clm_args=["-ms":opts.clm_args]}
	opts
# (modules,w) = findModules (fromMaybe ["."] opts.directories) w
| isError modules = exit (snd (fromError modules) +++ " while finding modules") opts w
# modules = if (isEmpty opts.modules)
	id
	(filter (\found -> any (\req -> endsWith ("" </> replaceSubString "." {pathSeparator} req <.> "dcl") ("" </> found)) opts.modules))
	(fromOk modules)
# w = seqSt (handleModule opts) modules w
= w
where
	optionDescription :: Option Options
	optionDescription = WithHelp True $ Options
		[ Shorthand "-d" "--directory" $ Option
			"--directory"
			(\dir opts -> Ok {Options | opts & directories= ?Just (fromMaybe [] opts.directories ++ [dir])})
			"DIR"
			"The directory to test modules from (default: .; can be given multiple times)"
		, Shorthand "-m" "--module" $ Option
			"--module"
			(\mod opts -> Ok {opts & modules=opts.modules ++ [mod]})
			"MOD"
			"Add MOD to the to-test modules -- when no modules are given, all modules from --directory are used"
		, Shorthand "-D" "--output-directory" $ Option
			"--output-directory"
			(\dir opts -> Ok {opts & output_directory=dir})
			"DIR"
			"Use DIR as directory for test modules (default: .)"
		, Shorthand "-p" "--prefix" $ Option
			"--prefix"
			(\p opts -> Ok {opts & output_prefix=p})
			"PREFIX"
			"The prefix for test module names (default: _Tests, i.e., Data.Set goes to _Tests.Data.Set)"
		, Shorthand "-I" "--include" $ Option
			"--include"
			(\dir opts -> Ok {opts & includes=opts.includes ++ [dir]})
			"DIR"
			"Add DIR to the include path"
		, Shorthand "-IL" "--include-library" $ Option
			"--include-library"
			(\lib opts -> Ok {opts & library_includes=opts.library_includes ++ [lib]})
			"LIB"
			"Add CLEAN_HOME/lib/LIB to the include path"
		, Shorthand "-H" "--clean-home" $ Option
			"--clean-home"
			(\h opts -> Ok {opts & clean_home=h})
			"PATH"
			"Set CLEAN_HOME to PATH (used to find libraries)"
		, Shorthand "-P" "--print-option" $ Option
			"--print-option"
			(\po opts -> Ok {opts & print_options=opts.print_options ++ [po]})
			"OPT"
			"Add OPT to the print options (see Gast's PrintOption type)"
		, Shorthand "-T" "--test-option" $ Option
			"--test-option"
			(\po opts -> Ok {opts & test_options=opts.test_options ++ [po]})
			"OPT"
			"Add OPT to the test options (see Gast's Testoption type)"
		, Shorthand "-c" "--compile" $ Flag
			"--compile"
			(\opts -> Ok {opts & compile=True})
			"Compile the tests after generation"
		, Shorthand "-C" "--clm-arg" $ Option
			"--clm-arg"
			(\arg opts -> Ok {opts & clm_args=opts.clm_args ++ [arg]})
			"ARG"
			"Add ARG to the command line arguments for clm"
		, Shorthand "-r" "--run" $ Flag
			"--run"
			(\opts -> Ok {opts & compile=True, run=True})
			"Run the tests after generation (implies --compile)"
		, Shorthand "-v" "--verbose" $ Flag
			"--verbose"
			(\opts -> Ok {opts & verbosity=inc opts.verbosity})
			"Increase verbosity (can be given multiple times)"
		, Shorthand "-q" "--quiet" $ Flag
			"--quiet"
			(\opts -> Ok {opts & verbosity=dec opts.verbosity})
			"Decrease verbosity (can be given multiple times)"
		, Flag "--no-color"
			(\opts -> Ok {opts & color=False})
			"Turn off color in output"
		]

exit :: !String !Options !*World -> *World
exit error opts w = setReturnCode 1 $ output ERROR error opts w

DEBUG   :== 5
SUCCESS :== 4
INFO    :== 3
WARNING :== 2
ERROR   :== 1

output :: !Int !String !Options !*World -> *World
output level s opts w
| level > opts.verbosity = w
# (io,w) = stdio w
= snd $ fclose (io <<< color <<< s <<< newline) w
where
	color
	| opts.color = case level of
		DEBUG   -> "\033[0;36m"
		INFO    -> "\033[0;34m"
		SUCCESS -> "\033[0;32m"
		WARNING -> "\033[0;33m"
		ERROR   -> "\033[0;31m"
	| otherwise = ""

	newline = if opts.color "\033[0m\n" "\n"

findModules :: ![FilePath] !*World -> *(!MaybeOSError [FilePath], !*World)
findModules [] w = (Ok [], w)
findModules [dir:dirs] w
# (files,w) = readDirectory dir w
| isError files = (liftError files, w)
# (mods,w) = mapSt recurse [fp \\ fp <- fromOk files | not $ isMember fp [".",".."]] w
# mods = flatten <$> sequence mods
| isError mods = (mods, w)
# (more,w) = findModules dirs w
= (((++) (fromOk mods)) <$> more, w)
where
	recurse :: !FilePath !*World -> *(!MaybeOSError [FilePath], !*World)
	recurse fp w
	# fp = dir </> fp
	# (info,w) = getFileInfo fp w
	| isError info = (Error (fromError info), w)
	| (fromOk info).directory = findModules [fp] w
	| otherwise = (Ok (if (endsWith ".dcl" fp) [fp] []), w)

handleModule :: !Options !FilePath !*World -> *World
handleModule opts fp w
# w = output DEBUG ("Checking " +++ fp +++ "...") opts w
// Find properties
#! (comments,w) = scanComments fp w
#! (dcl,w) = readModule fp w
#! (mod,dcldefs,documentation) = case dcl of
	Error _ -> (abort "failed to parse module\n", [], emptyCollectedComments)
	Ok (dcl,_) -> (dcl, dcl.mod_defs, case comments of
		Error _ -> emptyCollectedComments
		Ok comments -> collectComments comments dcl)
# (fundefs,instancedefs,typedefs) = get_checkable_definitions documentation dcldefs
# moddoc = case parseDoc <$> getComment mod documentation of
	?Just (Right (doc,_)) -> ?Just doc
	_                     -> ?None
# modname = mod.mod_ident.id_name
# output_modname = opts.output_prefix +++ "." +++ modname
# output_filename = opts.output_directory </> replaceSubString "." {pathSeparator} output_modname +++ ".icl"
# (nprops,coverage,props) = generatePropertyModule
	output_modname modname
	opts.print_options
	opts.test_options
	moddoc
	fundefs instancedefs typedefs
// Write properties
| nprops == 0 = w
# w = output INFO
	("Found " +++ pluralisen English nprops "test case" +++
		" in module " +++ modname +++
		"; " <+ entier (coverage * 100.0) <+ "% coverage") opts w
# (dir,_) = splitFileName output_filename
# (ok,w) = ensureDirectoryExists dir w
| isError ok = exit (snd (fromError ok) <+ " " +++ output_filename) opts w
# (mbCurrentContents,w) = readFile output_filename w
# (ok,w) = if (isOk mbCurrentContents && fromOk mbCurrentContents == props)
	(Ok (), w) // Don't overwrite so that the module doesn't need to be recompiled
	(writeFile output_filename props w)
| isError ok = exit (fromError ok <+ " " +++ output_filename) opts w
// Compile tests
| not opts.compile = w
# w = output INFO ("Compiling " +++ output_modname +++ "...") opts w
// First recompile _system, because we may use different codegen options (clm does not do this automatically)
# (ok,w) = callProcessAndPassIO "clm"
	([opt \\ opt <- opts.clm_args | opt <> "-fusion" && opt <> "-generic_fusion"] ++
		interleave "-I" [opts.output_directory : fromMaybe ["."] opts.directories ++ opts.includes] ++
		interleave "-IL" opts.library_includes ++
		["-O", "_system"]) ?None w
| isError ok = exit (snd (fromError ok) <+ " during code generation for _system") opts w
| fromOk ok <> 0 = exit "Code generation for _system finished with non-zero exit code" opts w
// Now we can compile the test module itself
# output_exename = opts.output_directory </> output_modname
  output_exename = IF_WINDOWS (output_exename +++ ".exe") output_exename
# (ok,w) = callProcessAndPassIO "clm"
	(IF_WINDOWS ["-no-redirect-stderr":opts.clm_args] opts.clm_args ++
		interleave "-I" [opts.output_directory : fromMaybe ["."] opts.directories ++ opts.includes] ++
		interleave "-IL" opts.library_includes ++
		[output_modname, "-o", output_exename]) ?None w
| isError ok = exit (snd (fromError ok) <+ " during compilation") opts w
| fromOk ok <> 0 = exit "Compilation finished with non-zero exit code" opts w
// Run tests
| not opts.run = w
# w = output INFO ("Running " +++ output_modname +++ "...") opts w
# (ok,w) = callProcessAndPassIO output_exename [] ?None w
| isError ok = exit (snd (fromError ok) <+ " while running the tests") opts w
| fromOk ok <> 0 = exit "Test finished with non-zero exit code" opts w
= output SUCCESS (output_modname +++ " passed") opts w
where
	interleave :: a [a] -> [a]
	interleave _ []     = []
	interleave g [x:xs] = [g,x:interleave g xs]

generatePropertyModule :: !String !String ![String] ![String] !(?ModuleDoc)
	![(String, FunctionDoc)]
	![(String, InstanceDoc)]
	![(String, TypeDoc)]
	-> (!Int, !Real, !String)
generatePropertyModule testmodname modname print_options test_options mod_doc fes ies tes
	= (n_props, coverage, tests)
where
	n_props = length props
	coverage = toReal (length (filter (not o isEmpty) propsets)) / toReal (length fes + length ies)
	tests = join "\n\n"
		[ "module " +++ testmodname
		, join "\n" $
			if default_imports
				[ "import Gast, Gast.CommandLine"
				, "from Testing.TestEvents import :: TestLocation{..}"
				, "from StdBool import ||"
				, "from StdMisc import abort"
				, "from StdString import instance toString {#Char}"
				, "import " +++ modname
				, ""
				, "//* Evaluate a property that we know will return only one Admin"
				, "check_property_ :: !Property -> Bool"
				, "check_property_ prop = case evaluate prop genState newAdmin of"
				, "\t[|{res}] -> res=:Pass || res=:OK"
				, "\t[|]      -> abort \"check_property_: no Admin returned\\n\""
				, "\t_        -> abort \"check_property_: more than one Admin returned\\n\""
				]
				[]
		, bootstrap
		, generators_string
		, invariants
		, start
		: [gp.gp_implementation \\ gp <- props]
		]

	propsets = handle fes ++ handle ies
	where
		handle :: ([(String,doc)] -> [[GeneratedProperty]]) | docPreconditions, docProperties, docPropertyTestWith doc
		handle = map (uncurry $ generateProperties modname tes pvis generators)
		pvis = fromMaybe [] $ docPropertyTestWith <$> mod_doc
	props = flatten propsets

	(bootstrap,default_imports) = case docPropertyBootstrap =<< mod_doc of
		?None ->
			("",True)
		?Just bs=:{bootstrap_content=MultiLine content} ->
			( content
			, not bs.bootstrap_without_default_imports
			)

	invariants = join "\n\n" $ concatMap (\(_,td) -> map invariant td.TypeDoc.invariants) tes
	where
		invariant :: Property -> String
		invariant (ForAll name args impl) =
			name +++ " :: " <+ Func (map noContext argtypes) (Type "Property" []) (TypeContext (concatMap context argtypes)) <+ "\n" +++
			name +++ concat [" " +++ a \\ (a,_) <- args] +++ " =\n\t" +++
			replaceSubString "\n" "\n\t" impl
		where
			noContext (Func [] t _) = t
			noContext t = t
			context (Func [] _ (TypeContext c)) = c
			context _ = []

			argtypes = map snd args

	generators = [("gast_generator_" <+ i,ptg) \\ i <- [0..] & ptg <- fromMaybe [] (docPropertyTestGenerators <$> mod_doc)]
	generators_string = join "\n\n" $ map makeGenerator $ generators
	where
		makeGenerator :: NamedTestGenerator -> String
		makeGenerator (name, ptg) = case ptg of
			PTG_Function t imp -> mkgen name t (imp % (3, size imp-1))
			PTG_List t imp -> mkgen name (Type "_!List" [t]) (zf +++ imp`)
			with
				zf = " = [|x \\\\ x <|- xs]\nwhere\n\txs = "
				imp` = replaceSubString "\n" "\n\nt" imp

		mkgen name type imp = join "\n"
			[ name +++ " :: " <+ type
			, name +++ imp
			]

	start = join "\n\t"
		[ "Start w = exposeProperties"
		, "[" +++ join "," print_options +++ "]"
		, "[" +++ join "," test_options +++ "]"
		, "[ EP " +++ join "\n\t, EP " [gp.gp_name \\ gp <- props]
		, "] w"
		]

:: GeneratedProperty =
	{ gp_name           :: !String
	, gp_implementation :: !String
	}

:: NamedTestGenerator :== (String, PropertyTestGenerator)

:: GeneratedArgument =
	{ ga_arg   :: !String // the name of the argument as stated
	, ga_names :: ![String] // generated argument names
	, ga_expr  :: !String // expression (for PTG_Function)
	, ga_ptg   :: !?NamedTestGenerator // the generator used
	}

generateProperties ::
	!String ![(String, TypeDoc)]
	![PropertyVarInstantiation] ![NamedTestGenerator] !String
	!doc
	-> [GeneratedProperty]
	| docPreconditions, docProperties, docPropertyTestWith doc
generateProperties modname type_defs pvis generators fname doc =
	[gen i fname (docPreconditions doc) p config
		\\ p <- docProperties doc
		, config <- configurationsForProperty p (pvis ++ docPropertyTestWith doc)
		& i <- [1..]]
where
	configurationsForProperty :: !Property ![PropertyVarInstantiation] -> [[(String,Type)]]
	configurationsForProperty (ForAll _ ts _) pvis =
		case [vi \\ PropertyVarInstantiation vi=:(v,_) <- pvis | isMember v allvs] of
			[]  -> [[]]
			vis -> configurations $ groupInstantiations vis
	where
		allvs = concatMap (allVars o snd) ts

		groupInstantiations :: [(String,Type)] -> [[(String,Type)]]
		groupInstantiations pvis = groupBy ((==) `on` fst) $ sortBy ((<) `on` fst) pvis

		configurations :: [[(String,Type)]] -> [[(String,Type)]]
		configurations [vis:viss] = [[vi`:vis`] \\ vi` <- vis, vis` <- configurations viss]
		configurations [] = [[]]

	gen :: !Int !String ![String] !Property ![(String,Type)] -> GeneratedProperty
	gen i fname preconditions (ForAll name ts imp) vis =
		{ gp_name = tname
		, gp_implementation = join "\n" $
			[ tname +++ " :: Property"
			, tname +++ " = location_and_name {moduleName= ?Just \"" +++ modname +++ "\"} \"" +++ fname +++ ": " +++ name` +++ "\""
			, "\t(" +++ join " " lambdas +++ join " " [name`` +++ "`":[ga.ga_expr \\ ga <- gas]] +++ {')' \\ _ <- lambdas} +++ ")"
			, "where"
			, "\t" +++ name`` +++ "` :: " +++ toString type`
			, "\t" +++ join " " [name`` +++ "`":[ga.ga_arg \\ ga <- gas]] +++ " = possibleFailReasons"
			, "\t\t[r \\\\ ?Just r <-"
			, "\t\t[ " +++ join "\n\t\t, " possible_fail_reasons
			] ++
			[ "\t\t]]"
			, "\t\t((True" +++ concat [" /\\ precondition_" <+ i <+ "_" \\ i <- [1..] & pre <- preconditions] +++ ") ==> actual_property_)"
			, "\twhere"
			, "\t\tactual_property_ = " +++ replaceSubString "\n" "\n\t\t\t" imp
			: ["\t\tprecondition_" <+ i <+ "_ = " +++ s \\ s <- preconditions & i <- [1..]]
			]
		}
	where
		tname = concat ["p",fixname fname,"_",fixname name,"_",toString i]
		name`` = "p" +++ fixname name`
		name` = if (i == 1) name (name +++ "_" +++ toString i)
		type` = fromJust $ assignAll vis $ Func (map snd ts) (Type "Property" []) (TypeContext [])

		possible_fail_reasons =
			[ concat
				[ "if (check_property_ (",invariant," ",arg,")) ?None "
				, "(?Just \"Invariant does not hold: ",invariant," ",arg,"\")"
				]
			\\ (arg,Type t _) <- ts
			, (type_name,type_doc) <- type_defs
			, ForAll invariant _ _ <- type_doc.invariants
			| t == type_name
			]

		gas = map resolveGenerators ts
		where
			resolveGenerators :: (String,Type) -> GeneratedArgument
			resolveGenerators (arg,t) =
				case [(n,is,ntg) \\ ntg=:(n,PTG_Function (Func is r _) _) <- generators | r == t] of
					[(n,is,ntg):_] ->
						{ ga_arg   = arg
						, ga_names = args
						, ga_expr  = "(" +++ join " " [n:args] +++ ")"
						, ga_ptg   = ?Just ntg
						}
						with args = [arg +++ "`" +++ toString i \\ _ <- is & i <- [0..]]
					[] -> case [(n,ntg) \\ ntg=:(n,PTG_List r _) <- generators | r == t] of
						[(n,ntg):_] ->
							{ ga_arg   = arg
							, ga_names = [arg]
							, ga_expr  = arg
							, ga_ptg   = ?Just ntg
							}
						[] ->
							{ ga_arg   = arg
							, ga_names = [arg]
							, ga_expr  = arg
							, ga_ptg   = ?None
							}

		lambdas = map makeLambda gas
		where
			makeLambda :: GeneratedArgument -> String
			makeLambda ga = case ga.ga_ptg of
				?Just (gen,PTG_List _ _)
					-> "ForEach " +++ gen +++ " (\\" +++ ga.ga_arg +++ "->"
					-> "\\" +++ join " " ga.ga_names +++ "->("

		fixname :: !String -> String
		fixname s = fix 0 s 0 (createArray (new_size 0 0 s) '\0')
		where
			new_size :: !Int !Int !String -> Int
			new_size i sz s
				| i >= size s
					= sz
				# c = s.[i]
				| isAlphanum c || c == ' '
					= new_size (i+1) (sz+1) s
					= new_size (i+1) (sz+3) s

			fix :: !Int !String !Int !*String -> .String
			fix i s j arr
				| i >= size s
					= arr
				# c = s.[i]
				| isAlphanum c
					= fix (i+1) s (j+1) {arr & [j]=c}
				| c == ' '
					= fix (i+1) s (j+1) {arr & [j]='_'}
				| otherwise
					# ci = toInt c
					# arr = {arr & [j]='`', [j+1]=hex (ci >> 4), [j+2]=hex (ci bitand 0xf)}
					= fix (i+1) s (j+3) arr

			hex :: !Int -> Char
			hex i = "0123456789abcdef".[i]
