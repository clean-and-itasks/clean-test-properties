implementation module cocl_helper

import StdEnv

from Clean.Doc import
	:: FunctionDoc,
	:: InstanceDoc,
	:: TypeDoc,
	:: DocBlock,
	:: ParseError,
	:: ParseWarning,
	parseDoc,
	traceParseWarnings, abortWithParseError,
	generic docBlockToDoc,
	derive docBlockToDoc FunctionDoc InstanceDoc TypeDoc
import Clean.Parse.Comments
import Clean.PrettyPrint
import Data.Either
import Data.List
from Text import class Text(concat), instance Text String

from syntax import
	:: FunKind,
	:: FunSpecials,
	:: Ident{id_name},
	:: IdentOrQualifiedIdent(..),
	:: Optional,
	:: ParsedDefinition(PD_Function,PD_Instance,PD_Instances,PD_Type,PD_TypeSpec),
	:: ParsedExpr,
	:: ParsedInstance{pi_class,pi_types},
	:: ParsedInstanceAndMembers{pim_pi},
	:: ParsedTypeDef,
	:: Position,
	:: Priority,
	:: Rhs,
	:: RhsDefsOfType,
	:: SymbolType,
	:: Type,
	:: TypeDef{td_ident}

get_checkable_definitions :: !CollectedComments ![ParsedDefinition] ->
	( ![(String, FunctionDoc)]
	, ![(String, InstanceDoc)]
	, ![(String, TypeDoc)]
	)
get_checkable_definitions documentation defs =
	( // Functions:
		[(id.id_name,doc) \\
			(pd,id) <-
				[(pd,id) \\ pd=:(PD_Function pos id _ _ _ _) <- defs] ++
				[(pd,id) \\ pd=:(PD_TypeSpec pos id _ _ _) <- defs],
			?Just docstring <- [getComment pd documentation],
			doc <- parseDocWithWarnings docstring]
	, // Instances:
		[(name,doc) \\
			(pd,pim_pi) <-
				[(pd,pim_pi) \\ pd=:(PD_Instance {pim_pi}) <- defs] ++
				[(pd,pim_pi) \\ pd=:(PD_Instances piams) <- defs, {pim_pi} <- piams],
			let name = concat ["instance ",ident_name pim_pi.pi_class," ":intersperse " " (map cppp pim_pi.pi_types)]
			, ?Just docstring <- [getComment pd documentation],
			doc <- parseDocWithWarnings docstring]
	, // Type definitions:
		[(id.id_name,doc) \\
			pd=:(PD_Type {td_ident=id}) <- defs,
			?Just docstring <- [getComment pd documentation],
			doc <- parseDocWithWarnings docstring]
	)
where
	parseDocWithWarnings s = case parseDoc s of
		Left e       -> abortWithParseError e
		Right (r,ws) -> traceParseWarnings ws [r]

	ident_name ident_or_qualified_ident = case ident_or_qualified_ident of
		Ident id              -> id.id_name
		QualifiedIdent _ name -> name
