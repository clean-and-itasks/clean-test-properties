definition module cocl_helper

from Clean.Doc import
	:: FunctionDoc,
	:: InstanceDoc,
	:: TypeDoc
from Clean.Parse.Comments import
	:: CollectedComments
from syntax import
	:: ParsedDefinition

get_checkable_definitions :: !CollectedComments ![ParsedDefinition] ->
	( ![(String, FunctionDoc)]
	, ![(String, InstanceDoc)]
	, ![(String, TypeDoc)]
	)
