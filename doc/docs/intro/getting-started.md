---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Getting started

To start using the property tester, you need to have a [Nitrile][] project.
Usually this is a library project, but you can also use the property tester for
applications.

See [here](https://clean-nc.gitlab.io/nitrile/getting-started) to set up a
Nitrile project.

To start, we have to add a
[`properties`](https://clean-nc.gitlab.io/nitrile/nitrile-yml/reference/#testsproperties)
test to `nitrile.yml`. We also need some dependencies:

## Setting up

```yml
dependencies:
  base: ^1.0
test_dependencies:
  property-tester: ^2.0
  test-runner: ^1.0
src:
  - src
tests:
  properties:
    properties: {}
```

Fetch the dependencies:

```txt
$ nitrile update
$ nitrile fetch
```

Create the following module in `src/Stack.dcl`:

```clean
definition module Stack

from StdOverloaded import class length

:: Stack a

instance length Stack

newStack :: Stack a
```

And the implementation module `src/Stack.icl`:

```clean
implementation module Stack

import StdEnv

:: Stack a = Stack [a]

instance length Stack
where
	length (Stack xs) = length xs

newStack :: Stack a
newStack = Stack []
```

## Your first test

You can now add a simple test to the definition module, in the documentation
block for `newStack`:

```clean
/**
 * @property is empty:
 *   length (newStack) =.= 0
 */
newStack :: Stack a
```

This defines a property with the name `is empty`. `=.=` is a Gast operator that
checks for equality.

Run the test:

```txt
$ nitrile test
Running properties...
Checking src/Stack.dcl...
Found 1 test case in module Stack; 100% coverage
Compiling _Tests.Stack...
Compiling _Tests.Stack
Overloading error [_Tests.Stack.icl,34,pis_empty`]: "==" no instance available of type Int
Compilation finished with non-zero exit code
property-tester exited with 1
```

Oh, we're lacking an instance. Let's add some bootstrap code at the top of the
definition module:

```clean
definition module Stack

/**
 * @property-bootstrap
 *   import StdEnv
 */
```

Rerun the test:

```txt
$ nitrile test
Running properties...
Checking src/Stack.dcl...
Found 1 test case in module Stack; 100% coverage
Compiling _Tests.Stack...
...
Linking _Tests.Stack
Started:  nitrile-tests/linux-x64/_Tests.Stack
Started:  newStack: is empty
Passed:   newStack: is empty
Passed:   nitrile-tests/linux-x64/_Tests.Stack
Passed.
```

Congratulations! The first test has passed.

## Adding universal quantification

Let's add some more functions:

```clean
fromList :: ![a] -> Stack a
push :: !a !(Stack a) -> Stack a
pop :: !(Stack a) -> ?(a, Stack a)
```

And in the implementation module:

```clean
fromList :: ![a] -> Stack a
fromList xs = Stack xs

push :: !a !(Stack a) -> Stack a
push x (Stack xs) = Stack [x:xs]

pop :: !(Stack a) -> ?(a, Stack a)
pop (Stack []) = ?None
pop (Stack [x:xs]) = ?Just (x, Stack xs)
```

Let's check that we can always `pop` from a stack produced by `fromList`. For
this, we need a universal quantifier `A.xs :: [Int]`. Gast will then generate
values `xs` of type `[Int]`, and you can use `xs` in the definition of the
property:

```clean
/**
 * @property can pop after fromList: A.xs :: [Int]:
 *   isJust (pop (fromList xs))
 */
pop :: !(Stack a) -> ?(a, Stack a)
```

(You also need to add `import StdMaybe` to the `@property-bootstrap` at the top
of the module.)

Run the test:

```txt
$ nitrile test
...
Started:  pop: can pop after fromList
Failed:   pop: can pop after fromList
  Counter-examples:
  - []
Failed:   nitrile-tests/linux-x64/_Tests.Stack
  Children tests failed: pop: can pop after fromList
```

Gast is right! We cannot `pop` from `fromList []`. Let's modify the property:

```clean
/**
 * @property can pop after fromList: A.xs :: [Int]:
 *   not (isEmpty xs) ==> isJust (pop (fromList xs))
 */
```

And this passes.

If you need to quantify over more variables, use `;`:

```clean
/**
 * @property pop returns the pushed element: A.xs :: [Int]; x :: Int:
 *   let result = pop (push x (fromList xs)) in
 *   isJust result /\ fst (fromJust result) =.= x
 */
push :: !a !(Stack a) -> Stack a
```

## Using type variables

It's not so neat that we hard-code the type `Int` in these tests. What we would
like to say is of course:

```clean
/**
 * @property pop returns the pushed element: A.xs :: [a]; x :: a:
 *   let result = pop (push x (fromList xs)) in
 *   isJust result /\ fst (fromJust result) =.= x
 */
push :: !a !(Stack a) -> Stack a

/**
 * @property can pop after fromList: A.xs :: [a]:
 *   not (isEmpty xs) ==> isJust (pop (fromList xs))
 */
```

But this gives us a bunch of errors:

```txt
Overloading error [_Tests.Stack.icl,50,ppop_returns_the_pushed_element`]: internal overloading of "=.=" could not be solved for: Eq a (context does not occur in function type)
Overloading error [_Tests.Stack.icl,50,ppop_returns_the_pushed_element`]: internal overloading of "=.=" could not be solved for: genShow{|*|} a (context does not occur in function type)
Overloading error [_Tests.Stack.icl,50,ppop_returns_the_pushed_element`]: internal overloading of "=.=" could not be solved for: gPrint{|*|} a (context does not occur in function type)
Overloading error [_Tests.Stack.icl,46,ppush_pop_returns_the_pushed_element_1]: internal overloading of "location_and_name" could not be solved for: TestArg _ (contains type variable not in function type at _ )
Overloading error [_Tests.Stack.icl,60,ppop_can_pop_after_fromList_1]: internal overloading of "location_and_name" could not be solved for: TestArg _ (contains type variable not in function type at _ )
```

The reason is that we have not specified which type Gast should use to generate
test values. The property tester allows you to do this centrally. On the module
documentation, you can add:

```clean
/**
 * @property-bootstrap
 *   ...
 *
 * @property-test-with a = Int
 * @property-test-with a = Char
 */
```

This tells the property tester to test twice: once with `Int`s, and once with
`Char`s. You will see that indexes are appended to the test names:

```txt
$ nitrile test
...
Started:  nitrile-tests/linux-x64/_Tests.Stack
Started:  newStack: is empty
Passed:   newStack: is empty
Started:  push: pop returns the pushed element
Passed:   push: pop returns the pushed element
Started:  push: pop returns the pushed element_2
Passed:   push: pop returns the pushed element_2
Started:  pop: can pop after fromList
Passed:   pop: can pop after fromList
Started:  pop: can pop after fromList_2
Passed:   pop: can pop after fromList_2
Passed:   nitrile-tests/linux-x64/_Tests.Stack
Passed.
```

## Using test generators

Above, we have hard-coded `fromList` in the test for `push`:

```clean
/**
 * @property pop returns the pushed element: A.xs :: [a]; x :: a:
 *   let result = pop (push x (fromList xs)) in
 *   isJust result /\ fst (fromJust result) =.= x
 */
push :: !a !(Stack a) -> Stack a
```

It would be nicer to write:

```clean
/**
 * @property pop returns the pushed element: A.xs :: Stack a; x :: a:
 *   let result = pop (push x xs) in
 *   isJust result /\ fst (fromJust result) =.= x
 */
push :: !a !(Stack a) -> Stack a
```

The problem is that Gast cannot generate values of this type:

```txt
$ nitrile test
...
Overloading error [_Tests.Stack.icl,48,ppush_pop_returns_the_pushed_element_1]: "genShow_s" no instance available of type (Stack Int)
Overloading error [_Tests.Stack.icl,48,ppush_pop_returns_the_pushed_element_1]: "ggen_s" no instance available of type (Stack Int)
```

There are a couple of ways to deal with this.

One obvious way is to add instances of the Gast functions in
`@property-bootstrap`. However, this is not always possible. For example, types
like `Map` and `Set` need a `<` instance for their type variable in order to
generate valid values for them.

In such cases, it is better to use a test generator. We can add it on the
module level:

```clean
/**
 * @property-bootstrap
 *   ...
 *
 * @property-test-generator [a] -> Stack a
 *   gen xs = fromList xs
 */
```

This defines a test generator from type `[a]` to type `Stack a`. (The
implementation must always define a function `gen`.)

When the property tester sees that a type used in a universal quantification
has a test generator, it will instruct Gast to generate values of the argument
of the test generator instead, and transform these using the generator
function.

Alternatively, it can be useful to just define a list of possible values for
Gast to choose from, like this:

```clean
/**
 * @property-test-generator list: Int
 *   [0..10]
 */
```

This shows that it is also possible to override Gast's default value generation
functions.

## Advanced properties

### Preconditions

Sometimes, it can be useful to describe something as a precondition for a
function. This can make properties easier to understand. For example, we had
the following (rather pointless) property:

```clean
/**
 * @property can pop after fromList: A.xs :: [a]:
 *   not (isEmpty xs) ==> isJust (pop (fromList xs))
 */
pop :: !(Stack a) -> ?(a, Stack a)
```

We can also write this as:

```clean
/**
 * @property can pop after fromList: A.xs :: [a]:
 *   isJust (pop (fromList xs))
 * @precondition not (isEmpty xs)
 */
pop :: !(Stack a) -> ?(a, Stack a)
```

Preconditions apply to *all* properties of a function, so it is also a way to
remove duplicate code.

### Invariants

Invariants are properties of types. They are defined on a type, and should
return a `Bool` or `Property`:

```clean
/**
 * @invariant can_pop_from_non_empty_stacks: A.xs :: Stack a:
 *   length xs > 0 <==> isJust (pop xs)
 */
:: Stack a
```

They are then plain functions that can be used in properties, for example:

```clean
/**
 * @property valid structure: A.xs :: [a]:
 *   can_pop_from_non_empty_stacks (fromList xs)
 */
fromList :: ![a] -> Stack a
```

This is useful if your type has some property that you want to check every time
a function produces a value of that type—for example, balancedness of binary
trees.

[Nitrile]: https://gitlab.com/clean-nc/nitrile
