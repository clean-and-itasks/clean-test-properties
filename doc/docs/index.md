---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Property tester

This is the documentation for the [Isocyanoclean][] [property tester][].

This tool can be used for automated testing of libraries. It collects [Gast][]
properties from definition modules and automatically generates test modules for
them.

Click on the "Next" link below to continue.

[![CC BY-NC-SA 4.0 license](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)  
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

For information about contributing, see [here](misc/about).

[Gast]: https://gitlab.com/clean-nc/libraries/gast
[Isocyanoclean]: https://clean-nc.camilstaps.nl
[property tester]: https://gitlab.com/clean-nc/tools/property-tester
