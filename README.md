# clean-test-properties

This tool can generate test programs using [Gast][]'s `exposeProperties` (see
above) from dcl modules. The tool was conceived in
[clean-platform#17](https://gitlab.com/clean-and-itasks/clean-platform/-/issues/19).

For documentation, see [here][doc].

## Installation

On Linux, the tool can be installed simply by running `make`. This builds the
dependencies of the Clean compiler as well. On Windows, you are yourself
responsible for this, but a project file is provided.

[doc]: https://clean-and-itasks.gitlab.io/clean-test-properties/
[Gast]: https://gitlab.com/clean-and-itasks/gast
